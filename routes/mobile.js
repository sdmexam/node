const { request, response } = require('express')
const express=require('express')
const router=express.Router()
const db=require('../db')
const utils=require('../utils')
/* id,companyname,model,price */
router.get('/all',(request,response)=>{
    const connection=db.openConnection()
    const statement=`select * from mobile`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})  
router.get('/:id',(request,response)=>{
    const connection=db.openConnection()
    const{id}=request.params
    const statement=`select *from mobile where id=${id}`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
router.get('/c/:companyname',(request,response)=>{
    const connection=db.openConnection()
    const{companyname}=request.params
    const statement=`select *from mobile where companyname='${companyname}'`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
/* id,companyname,model,price */
router.post('/add',(request,response)=>{
    const connection=db.openConnection()
    const{id,companyname,model,price}=request.body
    const statement=`insert into mobile (id, companyname, model, price) 
    values(${id},'${companyname}','${model}',${price})`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})

router.delete('/:id',(request,response)=>{
    const connection=db.openConnection()
    const{id}=request.params
    const statement=`delete from mobile where id=${id}`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
router.put('/:id',(request,response)=>{
    const connection=db.openConnection()
    const{id}=request.params
    const{price}=request.body
    const statement=`update mobile set price=${price} where id=${id}`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
module.exports=router;